provider "aws" {
  region = var.aws_region
}

# VPC
module "vpc" {
  source = "./vpc"

  aws_region         = var.aws_region
  availability_zones = var.availability_zones
  project_name       = var.project_name
  cluster_name       = var.cluster_name
}

# Load Balancer
module "nlb" {
  source = "./nlb"

  vpc_id            = module.vpc.vpc_id
  subnet_ids        = module.vpc.public_subnet_ids
  security_group_id = module.vpc.public_security_group_id
}

# Nodes
module "nodes" {
  source = "./nodes"

  subnet_ids           = module.vpc.public_subnet_ids
  security_group_id    = module.vpc.public_security_group_id
  nlb_http_tg_arn = module.nlb.nlb_http_tg_arn
  nlb_https_tg_arn = module.nlb.nlb_https_tg_arn
}
