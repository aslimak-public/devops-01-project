data "cloudinit_config" "worker_config" {
  gzip          = true
  base64_encode = true

  part {
    filename     = "init_worker.sh"
    content_type = "text/x-shellscript"
    content      = file("${path.module}/init_worker.sh")
  }
}

resource "aws_launch_template" "worker" {
  name = "${var.cluster_name}-worker-lt"

  image_id      = "ami-0d118c6e63bcb554e"
  instance_type = "t3.medium"

  key_name = "key1"

  iam_instance_profile {
    name = aws_iam_instance_profile.node.name
  }

  block_device_mappings {
    device_name = "/dev/sda1"
    ebs {
      volume_size           = "10"
      volume_type           = "gp3"
      delete_on_termination = true
    }
  }

  network_interfaces {
    associate_public_ip_address = true
    security_groups             = [var.security_group_id]
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "${var.cluster_name}-worker"
    }
  }

  user_data = data.cloudinit_config.worker_config.rendered
}

resource "aws_autoscaling_group" "workers" {
  name = "${var.cluster_name}-workers-asg"

  desired_capacity = 1
  min_size         = 1
  max_size         = 3
  force_delete     = true

  vpc_zone_identifier = var.subnet_ids

  launch_template {
    id      = aws_launch_template.worker.id
    version = "$Latest"
  }

  target_group_arns = [var.nlb_http_tg_arn, var.nlb_https_tg_arn]
}
