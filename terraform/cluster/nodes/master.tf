data "cloudinit_config" "master_config" {
  gzip          = true
  base64_encode = true

  part {
    filename     = "init_master.sh"
    content_type = "text/x-shellscript"
    content      = file("${path.module}/init_master.sh")
  }
}

resource "aws_instance" "master" {
  ami                  = "ami-0d118c6e63bcb554e"
  instance_type        = "t3.medium"
  key_name = "key1"

  iam_instance_profile = aws_iam_instance_profile.node.name
  user_data            = data.cloudinit_config.master_config.rendered

  subnet_id                   = var.subnet_ids[0]
  vpc_security_group_ids      = [var.security_group_id]
  associate_public_ip_address = true

  ebs_block_device {
    device_name           = "/dev/sda1"
    volume_size           = "10"
    volume_type           = "gp3"
    delete_on_termination = true
  }

  tags = {
    Name = "${var.cluster_name}-master"
  }

  lifecycle {
    ignore_changes = [
      ami,
      user_data
    ]
  }
}
