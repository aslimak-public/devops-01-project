variable "subnet_ids" {
  type = list(string)
}

variable "security_group_id" {
  type = string
}

variable "nlb_http_tg_arn" {
  type = string
}

variable "nlb_https_tg_arn" {
  type = string
}

variable "cluster_name" {
  type    = string
  default = "k8s-cluster"
}