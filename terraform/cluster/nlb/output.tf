output "nlb_http_tg_arn" {
  value = aws_lb_target_group.http.arn
}

output "nlb_https_tg_arn" {
  value = aws_lb_target_group.https.arn
}
