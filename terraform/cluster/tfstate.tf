# Save terraform state in s3 bucket
terraform {
  backend "s3" {
    bucket = "devops-project-bucket"
    key    = "terraform/devops-01-project-cluster.tfstate"
    region = "eu-central-1"

    dynamodb_table = "devops-project-state-locks"
    encrypt        = true
  }
}
